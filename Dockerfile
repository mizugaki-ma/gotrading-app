FROM golang:1.17.7-alpine as dev

ENV ROOT=/go/src/work
ENV USER=user1
# ENV CGO_ENABLED 0

WORKDIR ${ROOT}

RUN apk update && apk --no-cache add ca-certificates git shadow
COPY go.mod go.sum entrypoint.sh ./

RUN adduser ${USER} ${USER} -h ${ROOT} -D

RUN go mod download

USER ${USER}
ENV HOME=${ROOT}

EXPOSE 8080
ENTRYPOINT ["./entrypoint.sh"]
CMD ["go", "run", "main.go"]

FROM golang:1.17.7-alpine as builder

ENV ROOT=/go/src/work
ENV USER=user1
WORKDIR ${ROOT}

RUN apk update && apk --no-cache add ca-certificates git shadow
COPY go.mod go.sum entrypoint.sh ./

RUN adduser ${USER} ${USER} -h ${ROOT} -D

RUN go mod download

COPY . ${ROOT}
RUN CGO_ENABLED=0 GOOS=linux go build -o /build/main -ldflags '-s -w'
RUN chmod +x -R /build
RUN chown -R ${USER}:${USER} ${ROOT}

USER ${USER}
ENV HOME=${ROOT}

EXPOSE 8080
ENTRYPOINT ["./entrypoint.sh"]
CMD ["/build/main"]

FROM alpine:3.15 as prod

ENV ROOT=/go/src/work
ENV USER=user1
WORKDIR ${ROOT}
RUN apk update && apk --no-cache add ca-certificates shadow

COPY --from=builder /build/main /build/main
COPY --from=builder ${ROOT}/app/view/chart.html ${ROOT}/app/view/chart.html
COPY --from=builder ${ROOT}/entrypoint.sh ${ROOT}/entrypoint.sh

#COPY --from=builder ${ROOT}/config/config.ini ${ROOT}/config/config.ini
#COPY --from=builder ${ROOT}/cloudsqlkey/cloudsqlkey.json ${ROOT}/cloudsqlkey/cloudsqlkey.json

RUN adduser ${USER} ${USER} -h ${ROOT} -D
RUN chmod +x -R /build
RUN chown -R ${USER}:${USER} ${ROOT}

USER ${USER}
ENV HOME=${ROOT}


EXPOSE 8080
ENTRYPOINT ["./entrypoint.sh"]
CMD ["/build/main"]