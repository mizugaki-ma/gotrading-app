package models

import (
	"bittrading/bitflyer"
	"fmt"
	"log"
	"time"
)

type Candle struct {
	ProductCode string        `json:"product_code"`
	Duration    time.Duration `json:"duration"`
	Time        time.Time     `json:"time"`
	Open        float64       `json:"open"`
	Close       float64       `json:"close"`
	High        float64       `json:"high"`
	Low         float64       `json:"low"`
	Volume      float64       `json:"volume"`
}

func NewCandle(productCode string, duration time.Duration, timeDate time.Time, open, close, high, low, volume float64) *Candle {
	return &Candle{
		productCode,
		duration,
		timeDate,
		open,
		close,
		high,
		low,
		volume,
	}
}

func (c *Candle) TableName() string {
	return GetCandleTableName(c.ProductCode, c.Duration)
}

func (c *Candle) Create() error {
	cmd := fmt.Sprintf(`
		INSERT INTO %s (time, open, close, high, low, volume) 
			VALUES ($1, $2, $3, $4, $5, $6)`, c.TableName(),
	)
	_, err := DbConnection.Exec(cmd,
		c.Time.Format(time.RFC3339), c.Open, c.Close, c.High, c.Low, c.Volume)
	if err != nil {
		log.Printf("action=Create, err=%s, TableName=%s  \n", err.Error(), c.TableName())
		return err
	}
	return nil
}

func (c *Candle) Save() error {
	cmd := fmt.Sprintf("UPDATE %s SET open = $1, close = $2, high = $3, low = $4, volume = $5 WHERE time = $6", c.TableName())
	_, err := DbConnection.Exec(cmd, c.Open, c.Close, c.High, c.Low, c.Volume, c.Time.Format(time.RFC3339))
	if err != nil {
		log.Printf("action=Save, err=%s, TableName=%s  \n", err.Error(), c.TableName())
		return err
	}
	return err
}

func GetCandle(productCode string, duration time.Duration, dateTime time.Time) *Candle {
	tableName := GetCandleTableName(productCode, duration)
	cmd := fmt.Sprintf("SELECT time, open, close, high, low, volume FROM  %s WHERE time = $1", tableName)
	row := DbConnection.QueryRow(cmd, dateTime.Format(time.RFC3339))
	var candle Candle
	err := row.Scan(&candle.Time, &candle.Open, &candle.Close, &candle.High, &candle.Low, &candle.Volume)
	if err != nil {
		log.Printf("action=GetCandle, err=%s, tablename=%s, datetime=%s \n", err.Error(), tableName, dateTime)
		return nil
	}
	return NewCandle(productCode, duration, candle.Time, candle.Open, candle.Close, candle.High, candle.Low, candle.Volume)
}

func CreateCandleWithDuration(ticker bitflyer.Ticker, productCode string, duration time.Duration) bool {
	// productcode = ticker.Productcode, why do you need to pass the "productCode" value separetely$
	currentCandle := GetCandle(productCode, duration, ticker.TruncateDateTime(duration))
	price := ticker.GetMidPrice()
	if currentCandle == nil {
		candle := NewCandle(productCode, duration, ticker.TruncateDateTime(duration),
			price, price, price, price, ticker.Volume)
		candle.Create()
		return true
	}

	if currentCandle.High <= price {
		currentCandle.High = price
	} else if currentCandle.Low >= price {
		currentCandle.Low = price
	}
	currentCandle.Volume += ticker.Volume
	currentCandle.Close = price
	currentCandle.Save()
	return false
}

func GetAllCandle(productCode string, duration time.Duration, limit int) (dfCandle *DataFrameCandle, err error) {
	tableName := GetCandleTableName(productCode, duration)
	cmd := fmt.Sprintf(`
		SELECT * FROM (
			SELECT time, open, close, high, low, volume
			FROM %s
			ORDER BY time DESC
			LIMIT $1				
		) AS dt  ORDER BY time ASC`, tableName)
	rows, err := DbConnection.Query(cmd, limit)
	if err != nil {
		log.Println("failed to get all candles:", err)
		return
	}
	defer rows.Close()

	dfCandle = &DataFrameCandle{
		ProductCode: productCode,
		Duration:    duration,
	}
	for rows.Next() {
		var candle Candle
		candle.ProductCode = productCode
		candle.Duration = duration

		rows.Scan(&candle.Time, &candle.Open, &candle.Close, &candle.High, &candle.Low, &candle.Volume)
		dfCandle.Candles = append(dfCandle.Candles, candle)
	}
	err = rows.Err()
	if err != nil {
		log.Println("action=GetAllCandle err=", err)
		return nil, err
	}
	return dfCandle, nil
}
