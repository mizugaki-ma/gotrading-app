package models

import (
	"bittrading/config"
	"context"
	"database/sql"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"time"

	_ "github.com/GoogleCloudPlatform/cloudsql-proxy/proxy/dialers/postgres"
	"github.com/GoogleCloudPlatform/cloudsql-proxy/proxy/proxy"
	goauth "golang.org/x/oauth2/google"
)

const (
	tableNameSignalEvents = "signal_events"
)

var DbConnection *sql.DB
var ctx context.Context

func GetCandleTableName(productCode string, duration time.Duration) string {
	return fmt.Sprintf("%s_%s", productCode, duration)
}

func init() {
	ctx = context.Background()
	dsn := fmt.Sprintf("host=%s user=%s dbname=%s password=%s sslmode=disable",
		config.Config.DbHost, config.Config.DbUser, config.Config.DbName, config.Config.DbPass)

	var err error
	DbConnection, err = sql.Open(config.Config.SQLDriver, dsn)
	if err != nil {
		log.Fatalln("Db connection err=", err)
	}
	// defer DbConnection.Close()

	// Cloud SQL proxy settings
	pathToCred := "/go/src/work/cloudsqlkey/cloudsqlkey.json"
	client, err := clientFromCredentials(ctx, pathToCred)
	if err != nil {
		log.Fatal(err)
	}
	proxy.Init(client, nil, nil)

	cmd := fmt.Sprintf(`
        CREATE TABLE IF NOT EXISTS %s (
            time timestamp without time zone PRIMARY KEY NOT NULL,
            product_code varchar,
            side varchar,
            price float,
            size float)`, tableNameSignalEvents)
	DbConnection.Exec(cmd)

	for _, duration := range config.Config.Durations {
		tableName := GetCandleTableName(config.Config.ProductCode, duration)
		c := fmt.Sprintf(`
            CREATE TABLE IF NOT EXISTS %s (
            time timestamp without time zone PRIMARY KEY NOT NULL,
            open float,
            close float,
            high float,
            low float,
			volume float)`, tableName)
		DbConnection.Exec(c)
	}
}

func clientFromCredentials(ctx context.Context, file string) (*http.Client, error) {
	const SQLScope = "https://www.googleapis.com/auth/sqlservice.admin"
	var client *http.Client

	all, err := ioutil.ReadFile(file)
	if err != nil {
		return nil, err
	}

	cfg, err := goauth.JWTConfigFromJSON(all, SQLScope)
	if err != nil {
		return nil, err
	}

	client = cfg.Client(ctx)

	return client, nil
}
