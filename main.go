package main

import (
	"bittrading/app/controllers"
	"bittrading/config"
	"bittrading/utls"
	"log"
)

func main() {
	utls.LoggingSettings(config.Config.LogPath, config.Config.LogFile)
	controllers.StreamIngestionData()
	log.Println(controllers.StartWebServer())
}
