#!/bin/sh
set -euo pipefail

# modify runnning user
usermod -u $(id -u) -o ${USER}
#groupmod -g $(id -g) ${USER}

# set config.ini from enviroment variables
mkdir /go/src/work/config/
cat << EOS > /go/src/work/config/config.ini
[web]
web_port = ${web_port}

[gotrading]
log_path = ${gotrading_log_path}
log_file = ${gotrading_log_file}
product_code = ${gotrading_product_code}
trade_duration = ${gotrading_trade_duration}
back_test = ${gotrading_back_test}
use_percent = ${gotrading_use_percent}
data_limit = ${gotrading_data_limit}
stop_limit_percent = ${gotrading_stop_limit_percent}
num_ranking = ${gotrading_num_ranking}

[bitflyer3]
api_key = ${api_key}
api_secret = ${api_secret}

[db]
db_host = ${db_host}
db_name = ${db_name}
db_user = ${db_user}
db_pass = ${db_pass}
db_driver = ${db_driver}
EOS

# set CloudSQL proxy keyfile from enviroment variables
mkdir /go/src/work/cloudsqlkey
cat <<EOF > /go/src/work/cloudsqlkey/cloudsqlkey.json
{
  "type": "${csql_type}",
  "project_id": "${csql_project_id}",
  "private_key_id": "${csql_private_key_id}",
  "private_key": "${csql_private_key}",
  "client_email": "${csql_client_email}",
  "client_id": "${csql_client_id}",
  "auth_uri": "${csql_auth_uri}",
  "token_uri": "${csql_token_uri}",
  "auth_provider_x509_cert_url": "${csql_auth_provider_x509_cert_url}",
  "client_x509_cert_url": "${csql_client_x509_cert_url}"
}
EOF

# begin the process
"$@"