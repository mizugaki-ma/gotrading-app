package utls

import (
	"io"
	"log"
	"os"
	"path/filepath"
)

// LoggingSetting is configset for application log.
func LoggingSettings(logPath, logFile string) {
	err := os.MkdirAll(logPath, 0777)
	if err != nil {
		log.Fatalf("Cant Make the directry for Logfile. err: %s", err.Error())
	}
	logFilePath := filepath.Join(logPath, logFile)
	logFileOpen, err := os.OpenFile(logFilePath, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0777) // read/write any
	if err != nil {
		log.Fatalf("Cant Operate my Logfile. err: %s", err.Error())
	}
	multiLogger := io.MultiWriter(os.Stdout, logFileOpen)
	log.SetFlags(log.Ldate | log.Ltime | log.Lshortfile)
	log.SetOutput(multiLogger)
}
