module bittrading

go 1.17

require github.com/markcheno/go-quote v0.0.0-20211116021555-bb34b59a97eb

require (
	cloud.google.com/go/compute v1.1.0 // indirect
	github.com/golang/groupcache v0.0.0-20200121045136-8c9f03a8e57e // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/googleapis/gax-go/v2 v2.1.1 // indirect
	github.com/jmoiron/sqlx v1.3.4 // indirect
	go.opencensus.io v0.23.0 // indirect
	go.uber.org/atomic v1.7.0 // indirect
	go.uber.org/multierr v1.6.0 // indirect
	go.uber.org/zap v1.20.0 // indirect
	golang.org/x/net v0.0.0-20211020060615-d418f374d309 // indirect
	golang.org/x/oauth2 v0.0.0-20211104180415-d3ed0bb246c8 // indirect
	golang.org/x/sys v0.0.0-20220128215802-99c3d69c2c27 // indirect
	golang.org/x/text v0.3.6 // indirect
	golang.org/x/time v0.0.0-20211116232009-f0f3c7e86c11 // indirect
	google.golang.org/api v0.66.0 // indirect
	google.golang.org/appengine v1.6.7 // indirect
	google.golang.org/genproto v0.0.0-20220114231437-d2e6a121cae0 // indirect
	google.golang.org/grpc v1.40.1 // indirect
	google.golang.org/protobuf v1.27.1 // indirect
)

require (
	github.com/GoogleCloudPlatform/cloudsql-proxy v1.28.1
	github.com/go-ini/ini v1.66.4 // indirect
	github.com/gorilla/websocket v1.5.0 // indirect
	github.com/lib/pq v1.10.4 // indirect
	github.com/markcheno/go-talib v0.0.0-20190307022042-cd53a9264d70 // indirect
	github.com/mattn/go-sqlite3 v1.14.11 // indirect
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c // indirect
)
